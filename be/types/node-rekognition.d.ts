export = index;
declare class index {
    constructor(AWSParameters: any);
    rekognition: any;
    s3: any;
    bucket: any;
    compareFaces(sourceImage: any, targetImage: any, threshold: any): any;
    createCollection(collectionId: any): any;
    deleteCollection(collectionId: any): any;
    detectFaces(image: any): any;
    detectLabels(image: any, threshold: any): any;
    detectModerationLabels(image: any, threshold: any): any;
    doCall(endpoint: any, params: any): any;
    getImageParams(image: any): any;
    indexFaces(collectionId: any, image: any): any;
    listFaces(collectionId: any): any;
    searchFacesByFaceId(collectionId: any, faceId: any, threshold: any): any;
    searchFacesByImage(collectionId: any, image: any, threshold: any): any;
    uploadToS3(imagePaths: any, folder: any): any;
}
