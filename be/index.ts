// import * as express from 'express';
import { AwsFaceRecognitionService } from './src/aws/aws.service';
const express = require('express')
import * as bodyParser from 'body-parser';
import * as path from 'path';
const multer = require('multer');
const fs = require("fs");
import { Request, Response } from 'express-serve-static-core';

console.log(`face recognizer is starting...`)

let awsFaceService = new AwsFaceRecognitionService();
const upload = multer({ dest: '/tmp' });

var cors = require('cors')

var app = express();
app.use(cors());
app.use(bodyParser.json());

app.get('/', function (req: any, res: any) {
    res.send('Face recognizer is up and running');
})
export interface MulterFile {
    key: string // Available using `S3`.
    path: string // Available using `DiskStorage`.
    mimetype: string
    originalname: string
    size: number
}

// curl -F 'file=@pictures/camera.jpg' 'http://localhost:3000/api/users/dk727g'
app.post('/api/users/:id', upload.single('file'), async function (req: Request & { file: MulterFile }, res: Response) {
    const id: string = req.params['id'];
    await fs.readFile(req.file.path, function (err: any, data: any) {
        let file: string = __dirname + "/tmp_pictures/" + req.file.originalname;
        fs.writeFile(file, data, function (err2: any) {
            if (err2 !== null) {
                console.error(err2);
                let responseObj = {
                    message: 'Sorry, file couldn\'t be uploaded.',
                    filename: req.file.originalname
                };
                res.send(JSON.stringify(responseObj));
                return;
            }
            let responseObj: any = {};
            try {
                awsFaceService.uploadImage(file)
                    .then((image) => {
                        awsFaceService.indexImage(image).then(() => {
                            responseObj = {
                                message: `Image for id: ${id} was successfully uploaded to AWS`,
                                filename: req.file.originalname,
                                file_on_server_side: file,
                            };
                            res.send(responseObj);
                            return;
                        });
                    });
            }
            catch (err2) {
                console.error(err2);
                responseObj = {
                    message: 'Sorry, file couldn\'t be uploaded.',
                    filename: req.file.originalname
                };
                res.send(JSON.stringify(responseObj));
                return;
            }
        });
    });
})
app.post('/api/authorize', upload.single('file'), async function (req: Request & { file: MulterFile }, res: Response) {
    const id: string = req.params['id'];
    await fs.readFile(req.file.path, function (err: any, data: any) {
        let file: string = __dirname + "/tmp_pictures/" + req.file.originalname;
        fs.writeFile(file, data, function (err2: any) {
            if (err2 !== null) {
                console.error(err2);
                let responseObj = {
                    message: 'Sorry, file couldn\'t be uploaded.',
                    filename: req.file.originalname
                };
                res.send(JSON.stringify(responseObj));
                return;
            }
            let responseObj: any = {};
            try {
                awsFaceService.uploadImage(file)
                    .then((image) => {
                        if (image !== null) {
                            awsFaceService.compareImages(image)
                                .then((sim) => {
                                    responseObj = {
                                        message: `Image for id: ${id} was successfully uploaded to AWS`,
                                        filename: req.file.originalname,
                                        file_on_server_side: file,
                                        similarity: sim
                                    };
                                    res.send(responseObj);
                                    awsFaceService.removeImage(image);
                                    return;
                                });
                        }
                    })
            }
            catch (err2) {
                console.error(err2);
                responseObj = {
                    message: 'Sorry, file couldn\'t be uploaded.',
                    filename: req.file.originalname
                };
                res.send(JSON.stringify(responseObj));
                return;
            }
        });
    });
})
app.get('/api/users/authorize', function (req: any, res: any) {
    res.send(`validating image. TODO about to authorize user`);
})

app.listen(3000)
console.log(`server is up`);

