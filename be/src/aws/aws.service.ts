import Rekognition from 'node-rekognition';

export class AwsFaceRecognitionService {


    // Set your AWS credentials
    public AWSParameters = {
        "AWS": {
            "accessKeyId": "AKIAJVMIRE4PX4ZGBLJQ",
            "secretAccessKey": "2l3rF8sQgAuSUiGzYl2cp7AZzITHLLb8BzZSjBZ/",
            "region": "us-east-2",
            "bucket": "hakaton2019faceewkognition",
            "ACL": "public-read-write" // optional
        },
        "threshold": 90,
        "collection": "drivers",
        "defaultFolder": "faces"
    }

    public rekognition = new Rekognition(this.AWSParameters.AWS)

    public async uploadImage(imagePath: string) {
        /**
         * Upload image or images array to S3 bucket into specified folder
         *
         * @param {Array.<string>|string} imagePaths 
         * @param {string} folder a folder name inside your AWS S3 bucket (it will be created if not exists)
         */
        try {
            const s3Images = await this.rekognition.uploadToS3(imagePath, this.AWSParameters.defaultFolder);
            return s3Images;
        }
        catch (e) {
            console.log('failed to upload image: ', e);
        }
    }

    public async createCollection() {
        /**
         * Creates a collection
         *
         * @param {string} collectionId 
         */

        try {
            const collection = await this.rekognition.createCollection(this.AWSParameters.collection);
            return collection;
        }
        catch (e) {
            console.log('failed to create collection: ', e);
        }
    }

    public async indexImage(s3Image: any) {
        /**
         * Detects faces in the input image and adds them to the specified collection
         *
         * @param {string} collectionId 
         * @param {Object} s3Image 
        */
        try {
            const obj = { 'S3Object': { 'Bucket': s3Image.Bucket, 'Name': s3Image.key } }
            await this.rekognition.indexFaces(this.AWSParameters.collection, s3Image)
            return s3Image;
        }
        catch (e) {
            console.log('failed to index image: ', e);
            return null;
        }
    }
    public async compareImages(s3Image: any) {
        /**
         * First detects the largest face in the image (indexes it), and then searches the specified collection for matching faces.
         *
         * @param {string} collectionId 
         * @param {Object} s3Image 
         * @param {number} threshold (optional. Defaults 90)
         */
        try {
            const faceMatches = await this.rekognition.searchFacesByImage(this.AWSParameters.collection, s3Image, this.AWSParameters.threshold);
            return faceMatches;
        }
        catch (e) {
            console.log('failed to compare images: ', e);
            return null;
        }
    }
    public async removeImage(image: any) {
        //
    }
  
}