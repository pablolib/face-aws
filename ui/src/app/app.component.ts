import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { from, Observable } from 'rxjs';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    @ViewChild("video")
    public video: ElementRef;

    @ViewChild("canvas")
    public canvas: ElementRef;

    public captureSCR: any;
    public authorized: boolean = false;

    public constructor(private http: HttpClient) {
        this.captureSCR = null;
    }

    public ngOnInit() {
    }

    public ngAfterViewInit() {
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            navigator.mediaDevices.getUserMedia({ video: true }).then(stream => {
                this.video.nativeElement.srcObject = stream;
                this.video.nativeElement.play();
            });
        }
    }


    public b64toFile(dataURI): File {
        // convert the data URL to a byte string
        const byteString = atob(dataURI.split(',')[1]);
    
        // pull out the mime type from the data URL
        const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]
    
        // Convert to byte array
        const ab = new ArrayBuffer(byteString.length);
        const ia = new Uint8Array(ab);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
    
        // Create a blob that looks like a file.
        const blob = new Blob([ab], { 'type': mimeString });
        blob['lastModifiedDate'] = (new Date()).toISOString();
        blob['name'] = 'file';
            
        // Figure out what extension the file should have
        switch(blob.type) {
            case 'image/jpeg':
                blob['name'] += '.jpg';
                break;
            case 'image/png':
                blob['name'] += '.png';
                break;
        }
        // cast to a File
        return <File>blob;
    }

    public dataURItoBlob(dataURI) {
        const byteString = window.atob(dataURI);
        const arrayBuffer = new ArrayBuffer(byteString.length);
        const int8Array = new Uint8Array(arrayBuffer);
        for (let i = 0; i < byteString.length; i++) {
          int8Array[i] = byteString.charCodeAt(i);
        }
        const blob = new Blob([int8Array], { type: 'image/png' });    
        return blob;
     }

    public onAddDriver() {
        const context = this.canvas.nativeElement.getContext("2d").drawImage(this.video.nativeElement, 0, 0, 400, 400);
        this.captureSCR = this.canvas.nativeElement.toDataURL('image/png');

        // const imageBlob = this.dataURItoBlob(this.captureSCR.split(',')[1]);
        // const imageFile = new File([imageBlob], 'file', { type: 'image/png' });
        const imageFile = this.b64toFile(this.captureSCR);

        const formData = new FormData();
        formData.append('file', imageFile, 'new_driver.png');
        const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
        this.http.post('http://localhost:3000/api/users/naniah', formData, { headers: headers }) 
            .subscribe(
                data => {
                    console.log('PUT Request is successful: ', data);
                },
                error => {
                    console.log('Failed to upload file: ', error);
                });
    }

    handleError(e: any): any {
        throw new Error('Method not implemented.');
    }

    public onStartCar() {
        const context = this.canvas.nativeElement.getContext("2d").drawImage(this.video.nativeElement, 0, 0, 400, 400);
        this.captureSCR = this.canvas.nativeElement.toDataURL('image/jpeg');
        this.captureSCR = this.captureSCR.replace('image/png', 'image/octet-stream'); 
        // const imageBlob = this.dataURItoBlob(this.captureSCR.split(',')[1]);
        // const imageFile = new File([imageBlob], 'file', { type: 'image/png' });
        const imageFile = this.b64toFile(this.captureSCR);

        const formData = new FormData();
        formData.append('file', imageFile, 'identify_driver.png');
        const headers = new HttpHeaders({ 'enctype': 'multipart/form-data' });
        this.http.post('http://localhost:3000/api/authorize', formData, { headers: headers }) 
        .subscribe(
            (data: any) => {
                console.log('PUT Request is successful: ', data);
                if (data.similarity !== null && data.similarity.FaceMatches.length) {
                    this.playAudio(true);
                    this.authorized = true;
                } else {
                    this.playAudio(false);
                    this.authorized = false;
                }
            },
            (error: any) => {
                console.log('Failed to upload file: ', error);
                this.playAudio(false);
            });
    }

    public playAudio(success: boolean) {
        const audio = new Audio();
        if (success) {
            audio.src = '../../../assets/sounds/StartCar.wav';
        } else {
            audio.src = '../../../assets/sounds/Alarm.wav';
        }
        audio.load();
        audio.play();
    }
}